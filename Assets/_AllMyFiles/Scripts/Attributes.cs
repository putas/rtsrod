using UnityEngine;

namespace Rodspace {
    [CreateAssetMenu (menuName = "RPG Generator/Player/Create Attribute")]
    public class Attributes : ScriptableObject
    {
        public string Description;
        public Sprite Thumbnail;
    }
}