using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rodspace {

    public class PlayerStats : MonoBehaviour {

        [Header("Main Player Stats")]
        public string PlayerName;
        public int PlayerXP = 0;
        public int PlayerLevel = 1;
        public int PlayerHP = 50; //baseline = 50, +5 each level

        [SerializeField]
        private int m_PlayerXP = 0;
        public int PlayerXP
        {
            get {return m_PlayerXP;}
            set
            {
                m_PlayerXP = value;

                //xp changed
                if (onXPChange != null)
                    onXPChange();
            }
        }

        //Set Listener for Player Level
        [SerializeField]
        private int m_PlayerLevel = 1;
        public int PlayerLevel
        {
            get {return m_PlayerLevel;}
            set 
            {
                m_PlayerLevel = value;

                if (onLevelChange != null)
                    onLevelChange();
            }
        }

        [Header("Player Attributes")]
        public List<PlayerAttributes> Attributes = new List<PlayerAttributes>();

        [Header("Player Skills Enabled")]
        public List<Skills> PlayerSkills = new List<Skills>();

        // Use this for initialization
        void Start () {

        }

        // Update
        void Update () {

        }

        //Delegates for listeners
        public delegate void OnXPChange();
        public event OnXPChange onXPChange;
        public delegate void OnLevelChange();
        public event OnLevelChange onLevelChange;

        //Temporary methods
        public void UpdateLevel (int amount)
        {
            PlayerLevel += amount;
        }

        public void UpdateXP (int amount)
        {
            PlayerXP += amount;
        }
    }
}