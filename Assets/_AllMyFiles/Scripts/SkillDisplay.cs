using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rodspace {
    public class SkillDisplay : Monobehaviour {
        
        //Get the Scriptable Object for Skill
        public Skills skills;
        //Get the UI components
        public Text skillName;
        public Text skillDescription;
        public Image skillIcon;
        public Text skillLevel;
        public Text skillXPNeeded;
        public Text skillAttribute;
        public Text skillAttrAmount;

        [SerializeField]
        private PlayerStats m_PlayerHandler;

        //Start
        void Start () 
        {

            m_PlayerHandler = this.GetComponentInParent<PlayerHandler>().Player;
            //listener for the XP change
            m_PlayerHandler.onXPChange += ReactToChange;
            //listener for the level change
            m_PlayerHandler.onLevelChange += ReactToChange;

            if (skill)
                skills.SetValues(this.gameObject, m_PlayerHandler);

            EnableSkills();

        }

        public void Enableskills()
        {
            //if player has the skill already, then show it as enabled
            if (m_PlayerHandler && skill && skill.EnableSkill(m_PlayerHandler))
            {
                TurnOnSkillIcon();
            }
            else if (m_PlayerHandler && skill && skill.CheckSkills(m_PlayerHandler))
            {
                this.GetComponent<Button>().interactable = true;
                this.transform.Find("IconParent").Find("Disabled").gameObject.SetActive(false);
            }
            else
            {
                TurnOffSkillIcon();
            }
        }

        private void OnEnable()
        {
            
        }

        //Update
        void Update () {

        }
    }
}