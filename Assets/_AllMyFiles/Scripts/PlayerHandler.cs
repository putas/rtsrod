using UnityEngine;

namespace Rodspace 
{
    public class PlayerHandler : MonoBehaviour
    {
        public PlayerStats Player;

        [SerializeField]
        private Canvas m_Canvas;
        private bool m_SeeCanvas;

        // Update

        void Updade()
        {
            // if you press the tab key
            if(Input.GetKeyDown("tab"))
            {
                if (m_Canvas)
                {
                    m_SeeCanvas = !m_SeeCanvas;
                    m_Canvas.gameObject.SetActive(m_SeeCanvas); // display or not the canvas
                }
            }
        }
    }
}